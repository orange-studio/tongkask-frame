<?php

namespace TongkaskFrame;

use Exception;

class TongkaskException extends Exception
{
    public const System_ERROR_CODE          = 33;  // 系统错误
    public const Request_Confine_ERROR_CODE = 44;  // 系统限制
    public const Request_Xxs_ERROR_CODE     = 55;  // XXS请求非法
    public const Router_ERROR_CODE          = 66;  // 路由方法不存在
    public const EasyPdo_ERROR_CODE         = 111; // 数据库PDO链接错误
    public const EasyRedis_ERROR_CODE       = 222; // redis链接错误
    public const EasyMQ_ERROR_CODE          = 333; // MQ链接错误
    public const Logger_ERROR_CODE          = 444; // 日志错误
    public const UtilTool_ERROR_CODE        = 555; // 工具集错误
    public const EastXls_ERROR_CODE         = 666; // Excel操作错误
    public const Pdo_Util_ERROR_CODE        = 777; // PdoUtil操作错误
    public const Pdo_Secure_ERROR_CODE      = 999; // 系统安全性告警
}