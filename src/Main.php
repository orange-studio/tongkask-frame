<?php

namespace TongkaskFrame;

use Dotenv\Dotenv;
use EasyTask\Task;
use TongkaskFrame\Frame\HttpServer;
use TongkaskFrame\Frame\TaskProcess;
use TongkaskFrame\Tool\ConfTool;

class Main
{
    public const CORAL_VERSION = '1.0.0';
    private string $command;
    private string $mode;
    private bool   $daemonize;
    private Task   $task;
    private        $HttpCallBack;
    private        $TaskCallBack;

    public function __construct($argc, $argv)
    {
        $dotenv = Dotenv::createImmutable(getcwd());
        $dotenv->load();

        $this->mode      = $argv[1] ?? '';
        $this->command   = $argv[2] ?? '';
        $this->daemonize = $argv[3] ?? false;
        if ($argc <= 1) {
            printf("%-10s\n", 'server');
            printf("%10s %10s\n", 'install', '安装框架');
            printf("%10s %10s\n", 'start', '启动服务');
            printf("%10s %10s\n", 'stop', '停止服务');
            printf("%10s %10s\n", 'restart', '重启服务');
            printf("%10s %10s\n", 'status', '服务状态');
            printf("%-10s\n", 'process');
            printf("%10s %10s\n", 'install', '安装框架');
            printf("%10s %10s\n", 'start', '启动服务');
            printf("%10s %10s\n", 'stop', '停止服务');
            printf("%10s %10s\n", 'restart', '重启服务');
            printf("%10s %10s\n", 'status', '服务状态');
            exit();
        }

        if (!in_array($this->mode, ['process', 'server'])) {
            printf('命令错误');
            printf("%-10s\n", 'process');
            printf("%10s %10s\n", 'install', '安装框架');
            printf("%10s %10s\n", 'start', '启动服务');
            printf("%10s %10s\n", 'stop', '停止服务');
            printf("%10s %10s\n", 'restart', '重启服务');
            printf("%10s %10s\n", 'status', '服务状态');
            exit();
        }

        if (!in_array($this->command, ['install', 'start', 'stop', 'restart', 'status']) && $this->mode == 'process') {
            printf('命令错误');
            printf("%-10s\n", 'process');
            printf("%10s %10s\n", 'install', '安装框架');
            printf("%10s %10s\n", 'start', '启动服务');
            printf("%10s %10s\n", 'stop', '停止服务');
            printf("%10s %10s\n", 'restart', '重启服务');
            printf("%10s %10s\n", 'status', '服务状态');
            exit();
        }

        if (!in_array($this->command, ['install', 'start', 'stop', 'restart', 'status']) && $this->mode == 'server') {
            printf('命令错误');
            printf("%-10s\n", 'server');
            printf("%10s %10s\n", 'install', '安装框架');
            printf("%10s %10s\n", 'start', '启动服务');
            printf("%10s %10s\n", 'stop', '停止服务');
            printf("%10s %10s\n", 'restart', '重启服务');
            printf("%10s %10s\n", 'status', '服务状态');
            exit();
        }
    }

    public function SetHttpCallBack(callable $HttpCallBack): void
    {
        $this->HttpCallBack = $HttpCallBack;
    }

    public function Run(): void
    {
        $this->task = new Task();
        // 是否守护进程启动
        $this->task->setDaemon($this->daemonize == '-d');
        // 设置系统时区
        $this->task->setTimeZone('Asia/Shanghai');

        switch ($this->mode) {
            case 'process':
                // 设置服务名称
                $this->task->setPrefix('TongkaskProcess');
                // 自动重启
                $this->task->setAutoRecover(true);
                $this->ProcessRun();
                break;
            case 'server':
                // 设置服务名称
                $this->task->setPrefix('TongkaskServer');
                // 不自动重启
                $this->task->setAutoRecover();
                $this->ServerRun();
        }
    }

    public function SetTaskCallBack(callable $TaskCallBack): void
    {
        $this->TaskCallBack = $TaskCallBack;
    }

    private function ProcessRun(): void
    {
        switch ($this->command) {
            case 'start':
                $this->ConsoleInfo();
                $TaskProcess = new TaskProcess();
                call_user_func($this->TaskCallBack, $TaskProcess);
                $TaskProcess->TaskRun($this->task);
                $this->task->start();
                break;
            case 'stop':
                $this->task->stop($this->daemonize == '-f');
                break;
            case 'restart':
                $this->task->stop($this->daemonize == '-f');
                $this->task->start();
                break;
            case 'status':
                $this->task->status();
                break;
            default:
                // 如果命令不被识别，抛出一个异常
                exit("未识别的命令: {$this->command}");
        }
    }

    private function ServerRun(): void
    {

        switch ($this->command) {
            case 'start':
                $this->ConsoleInfo();
                $this->LoadServer();
                $this->task->start();
                break;
            case 'stop':
                $this->task->stop($this->daemonize == '-f');
                break;
            case 'restart':
                $this->task->stop($this->daemonize == '-f');
                $this->task->start();
                break;
            case 'status':
                $this->task->status();
                break;
            default:
                // 如果命令不被识别，抛出一个异常
                exit("未识别的命令: {$this->command}");
        }
    }

    private function LoadServer(): void
    {
        if (empty($this->HttpCallBack)) {
            exit('请设置HttpCallBack');
        }

        $this->task->addFunc(function () {
            call_user_func($this->HttpCallBack, new HttpServer());
        }, 'TongkaskServer', 0);
    }

    private function ConsoleInfo(): void
    {
        $systemInfo = '_____ _____ _   _ _____  _   __  ___   _____ _   __' . "\n";
        $systemInfo .= '|_   _|  _  | \ | |  __ \| | / / / _ \ /  ___| | / /' . "\n";
        $systemInfo .= '  | | | | | |  \| | |  \/| |/ / / /_\ \\ `--.| |/ /' . "\n";
        $systemInfo .= '  | | | | | | . ` | | __ |    \ |  _  | `--. \    \\' . "\n";
        $systemInfo .= '  | | \ \_/ / |\  | |_\ \| |\  \| | | |/\__/ / |\  \\' . "\n";
        $systemInfo .= '  \_/  \___/\_| \_/\____/\_| \_/\_| |_/\____/\_| \_/' . "\n";
        printf("\033[31m %s \033[0m\n", $systemInfo);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['MODE', $this->mode]);
        if ($this->mode == 'server') {
            vprintf("\033[31m%-20s%20s\033[0m\n", ['LISTEN_ADDR', ConfTool::GetConfig('server.addr')]);
            vprintf("\033[31m%-20s%20s\033[0m\n", ['LISTEN_PORT', ConfTool::GetConfig('server.port')]);
        }

        vprintf("\033[31m%-20s%20s\033[0m\n", ['RUN_MODE', $_ENV['ENV']]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['PHP_VERSION', PHP_VERSION]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['SWOOLE_VERSION', SWOOLE_VERSION]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['TONGKASK_VERSION', self::CORAL_VERSION]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['REDIS_VERSION', phpversion('redis')]);
        vprintf("\033[31m%-20s%20s\033[0m\n", ['XLSWRITER_VERSION', phpversion('xlswriter')]);
    }
}