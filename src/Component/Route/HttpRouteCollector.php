<?php

namespace TongkaskFrame\Component\Route;

use Closure;
use Exception;
use TongkaskFrame\Frame\HttpBasic\HttpRequest;
use TongkaskFrame\Struct\RouteMiddle;
use TongkaskFrame\Struct\RouteStruct;
use TongkaskFrame\TongkaskException;
use TongkaskFrame\Tool\UtilTool;

class HttpRouteCollector
{
    public const NOT_FOUND          = 0;
    public const FOUND              = 1;
    public const METHOD_NOT_ALLOWED = 2;
    public const GET                = 'GET';
    public const POST               = 'POST';
    public const PUT                = 'PUT';
    public const DELETE             = 'DELETE';
    public const PATCH              = 'PATCH';
    public const HEAD               = 'HEAD';
    public array    $Routes           = [];
    private array   $MethodType       = [self::GET, self::POST, self::PUT, self::DELETE, self::PATCH, self::HEAD];
    private string  $path             = '';
    private string  $group            = '';
    private array   $groupMiddleware  = [];
    private array   $globalMiddleware = [];
    private Closure $NotFoundCallBack;
    private Closure $NotAllowCallBack;

    public function __construct()
    {
        $this->NotFoundCallBack = function () { };
        $this->NotAllowCallBack = function () { };
    }

    public function AddGroup($path, callable $callback): void
    {
        $this->group = $path;
        $callback($this);
        $this->group           = '';
        $this->groupMiddleware = [];
    }

    /**
     * @throws Exception
     */
    public function AddGroupMiddleware(string $class, string $action): HttpRouteCollector
    {
        $Middleware         = new RouteMiddle();
        $Middleware->class  = $class;
        $Middleware->action = $action;
        if (!method_exists($Middleware->class, $Middleware->action)) {
            throw new TongkaskException("{$Middleware->class}::{$Middleware->action} is not exist");
        }
        $this->groupMiddleware[] = $Middleware;
        return $this;
    }

    /**
     * @throws Exception
     */
    public function AddGlobalMiddleware(string $class, string $action): void
    {
        $RouteMiddle         = new RouteMiddle();
        $RouteMiddle->class  = $class;
        $RouteMiddle->action = $action;
        if (!method_exists($RouteMiddle->class, $RouteMiddle->action)) {
            throw new TongkaskException("{$RouteMiddle->class}::{$RouteMiddle->action} is not exist");
        }
        $this->globalMiddleware[] = $RouteMiddle;
    }

    /**
     * @throws Exception
     */
    public function GET($path, $class, $action): HttpRouteCollector
    {
        $this->AddRoute(self::GET, $path, $class, $action);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function POST($path, $class, $action): HttpRouteCollector
    {
        $this->AddRoute(self::POST, $path, $class, $action);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function AddRoute($method, $path, $class, $action): HttpRouteCollector
    {
        if (!in_array($method, $this->MethodType)) {
            throw new Exception('The request mode is not supported');
        }
        $this->path = UtilTool::NormalizedPath($this->group . DIRECTORY_SEPARATOR . $path);
        if (in_array($this->path, array_keys($this->Routes))) {
            throw new Exception($this->path . '  Route already exists');
        }
        $RouterStruct = new RouteStruct();
        $Middleware   = [];
        if (!empty($this->globalMiddleware)) {
            $Middleware = array_merge($Middleware, $this->globalMiddleware);
        }
        $Middleware           = array_merge($Middleware, $this->groupMiddleware);
        $RouterStruct->method = strtoupper($method);
        $RouterStruct->path   = UtilTool::NormalizedPath($this->group . DIRECTORY_SEPARATOR . $path);
        $RouterStruct->class  = $class;
        $RouterStruct->action = $action;
        if (!method_exists($RouterStruct->class, $RouterStruct->action)) {
            throw new TongkaskException("{$RouterStruct->class}::{$RouterStruct->action} is not exist");
        }
        $RouterStruct->Middleware          = $Middleware;
        $this->Routes[$RouterStruct->path] = $RouterStruct;

        return $this;
    }

    /**
     * @throws Exception
     */
    public function AddMiddleware($class, $action): HttpRouteCollector
    {
        if (empty($this->Routes[$this->path])) {
            throw new Exception($this->path . '  Route does not exist');
        }
        if ($this->Routes[$this->path] instanceof RouteStruct) {
            $RouterMiddleware         = new RouteMiddle();
            $RouterMiddleware->class  = $class;
            $RouterMiddleware->action = $action;
            if (!method_exists($RouterMiddleware->class, $RouterMiddleware->action)) {
                throw new TongkaskException("{$RouterMiddleware->class}::{$RouterMiddleware->action} is not exist");
            }
            if (in_array($RouterMiddleware, $this->Routes[$this->path]->Middleware)) {
                throw new Exception($this->path . '  Duplicate middleware exists in the route. Procedure');
            }
            $this->Routes[$this->path]->Middleware[] = $RouterMiddleware;
        }
        return $this;
    }

    public function SetNotFoundCallBack(callable $callback): void
    {
        $this->NotFoundCallBack = $callback;
    }

    public function SetNotAllowCallBack(callable $callback): void
    {
        $this->NotAllowCallBack = $callback;
    }

    public function Dispatch(HttpRequest $request, string $url, string $method): array
    {
        if (empty($this->Routes[$url])) {
            call_user_func($this->NotFoundCallBack, $request);
            return [self::NOT_FOUND, null];
        }
        $route = $this->Routes[$url];
        if ($route instanceof RouteStruct) {
            if ($route->method != strtoupper($method)) {
                call_user_func($this->NotAllowCallBack, $request);
                return [self::METHOD_NOT_ALLOWED, null];
            }
        }

        return [self::FOUND, $route];
    }

    public function GetPrintRoute(): void
    {
        foreach ($this->Routes as $route) {
            if ($route instanceof RouteStruct) {
                $Middlewarestr = '';
                foreach ($route->Middleware as $Middleware) {
                    if ($Middleware instanceof RouteMiddle) {
                        $Middlewarestr .= $Middleware->class . '::' . $Middleware->action . '    ';
                    }
                }
                echo("[{$route->method}]  {$route->path} {$route->class} {$route->action} " . $Middlewarestr . "\n");
            }
        }
    }
}