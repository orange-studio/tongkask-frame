<?php

namespace TongkaskFrame\Component;

use Exception;
use Throwable;
use TongkaskFrame\TongkaskException;
use TongkaskFrame\Tool\ConfTool;
use TongkaskFrame\Tool\FileDirTool;
use TongkaskFrame\Tool\UtilTool;

class Logger
{
    const SUCCESS  = 0;
    const INFO     = 1;
    const WARNING  = 2;
    const ERROR    = 3;
    const CRITICAL = 4;
    const DEBUG    = 5;
    private ?int   $cutSize     = 0;
    private string $TraceID     = '';
    private bool   $isJsonPrint = false;
    private string $log         = '';
    private string $logDir      = '/Log';
    private string $logDateDir  = '';
    private string $fileName    = 'system.log';
    private bool   $output      = false;
    private string $lockFile    = 'composer.json';

    public function __construct()
    {

    }

    /**
     * @throws TongkaskException
     */
    public function SetInitModuleConfig($fileName, string $module = ''): Logger
    {
        $LogConfig    = ConfTool::GetConfig('Log');
        $this->output = $LogConfig['output'] ?? false;
        if (empty($module)) {
            $this->logDir     = TONGKASK_ROOT . DIRECTORY_SEPARATOR . $LogConfig['path'];
            $this->logDateDir = $this->logDir;
        } else {
            $this->logDir     = TONGKASK_ROOT . DIRECTORY_SEPARATOR . $LogConfig['path'] . '/' . $module;
            $this->logDateDir = UtilTool::NormalizedPath($this->logDir . DIRECTORY_SEPARATOR . date('y-m-d', time()));
        }


        $this->lockFile = TONGKASK_ROOT . DIRECTORY_SEPARATOR . $LogConfig['file_lock'];
        $this->filePathIsExist();
        if (empty($fileName)) {
            $this->fileName = $fileName . '.log';
        }
        $this->SetIsJsonPrint($LogConfig['json_print'] ?? false);
        $this->SetCutSize($LogConfig['cut_size'] ?? 0);
        return $this;
    }

    /**
     * @throws TongkaskException
     */
    public function filePathIsExist(): void
    {
        try {
            if (!empty($this->lockFile)) {
                if (!is_dir($this->logDateDir)) {
                    $file = fopen($this->lockFile, 'r');
                    if ($file) {
                        if (flock($file, LOCK_EX | LOCK_NB)) {
                            mkdir($this->logDateDir, 0777, true);
                            flock($file, LOCK_UN);
                        }
                    }
                    fclose($file);
                }
            } else {
                if (!is_dir($this->logDateDir)) {
                    mkdir(iconv('UTF-8', 'GBK', $this->logDateDir), 0777, true);
                }
            }
        } catch (Throwable $th) {
            if (!str_contains($th->getMessage(), 'mkdir')) {
                throw new TongkaskException($th->getMessage(), TongkaskException::Logger_ERROR_CODE);
            }
        }
    }

    public function SetIsJsonPrint(bool $isJsonPrint): Logger
    {
        $this->isJsonPrint = $isJsonPrint;
        return $this;
    }

    public function SetCutSize(int $cutSize = 50 * 1024 * 1024): Logger
    {
        $this->cutSize = $cutSize;
        return $this;
    }

    /**
     * @throws TongkaskException
     */
    public function SetInitFileConfig($fileName): Logger
    {
        $LogConfig        = ConfTool::GetConfig('Log');
        $this->output     = $LogConfig['output'] ?? false;
        $this->logDir     = TONGKASK_ROOT . DIRECTORY_SEPARATOR . $LogConfig['path'];
        $this->logDateDir = UtilTool::NormalizedPath($this->logDir);
        $this->lockFile   = TONGKASK_ROOT . DIRECTORY_SEPARATOR . $LogConfig['file_lock'];
        $this->filePathIsExist();
        $this->fileName = $fileName . '.log';
        $this->SetIsJsonPrint($LogConfig['json_print'] ?? false);
        $this->SetCutSize($LogConfig['cut_size'] ?? 0);
        return $this;
    }

    public function GetLogStr(): string
    {
        return $this->log;
    }

    public function SetTraceID(string $TraceID): Logger
    {
        $this->TraceID = $TraceID;
        return $this;
    }

    /**
     * @throws TongkaskException
     */
    public function writeLog($log, $lev = self::INFO): void
    {
        try {
            $this->appendLog($log, $lev);
            $this->write();
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws TongkaskException
     */
    public function appendLog($log, $lev = self::INFO, bool $isShowTime = true): void
    {
        try {
            $logString = '';
            if (!empty($lev) && $lev !== 0) {
                switch ($lev) {
                    case self::SUCCESS:
                        $logString = 'SUCCESS';
                        break;
                    case self::INFO:
                        $logString = 'INFO';
                        break;
                    case self::WARNING:
                        $logString = 'WARNING';
                        break;
                    case self::ERROR:
                        $logString = 'ERROR';
                        break;
                    case self::CRITICAL:
                        $logString = 'CRITICAL';
                        break;
                    case self::DEBUG:
                        $logString = 'DEBUG';
                        break;
                }
            }
            if (is_array($log)) {
                if ($this->isJsonPrint) {
                    $log = json_encode($log, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                } else {
                    $log = json_encode($log, JSON_UNESCAPED_UNICODE);
                }
            }
            $date      = date('Y-m-d H:i:s');
            $data      = $isShowTime ? "[{$date}] " : '';
            $logString = empty($logString) ? '' : "[{$logString}] ";
            if (empty($this->TraceID)) {
                $log = "{$data}{$logString} {$log}\n";
            } else {
                $log = "{$data}{$logString}[{$this->TraceID}] {$log}\n";
            }
            $this->log .= $log;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws TongkaskException
     */
    public function write(bool $parcel = true): void
    {
        try {
            $this->filePathIsExist();
            if ($parcel) {
                $startLog = '--[' . date('Y-m-d H:i:s') . "]--start-----------------------------------------\n";
                $endLog   = "--end------------------------------------------------------------------\n";

                $this->log = $startLog . $this->log . $endLog;
            }

            $this->PutContents();
            if ($this->output) {
                echo($this->log);
            }
            $this->log = '';
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::Logger_ERROR_CODE);
        }
    }

    public function PutContents(): void
    {
        $logFileName = UtilTool::NormalizedPath($this->logDateDir . DIRECTORY_SEPARATOR . $this->fileName);
        $log         = $this->log;
        if (file_exists($logFileName) && filesize($logFileName) > $this->cutSize && $this->cutSize != 0) {
            $goFileNamePath = UtilTool::NormalizedPath($this->logDateDir . DIRECTORY_SEPARATOR . $this->fileName);
            $toFileNamePath = UtilTool::NormalizedPath($this->logDateDir . DIRECTORY_SEPARATOR . date('ymdHis') . '_' . uniqid() . '_' . $this->fileName);
            FileDirTool::moveFile($goFileNamePath, $toFileNamePath, false);
        }
        file_put_contents($logFileName, $log, FILE_APPEND | LOCK_EX);
    }

    /**
     * @throws Exception
     */
    public function info($log): void
    {
        $this->writeLine($log);
    }

    /**
     * @throws TongkaskException
     */
    public function writeLine($log, $lev = self::INFO): void
    {
        try {
            $this->filePathIsExist();
            $this->appendLog($log, $lev);
            //  $logFileName = $this->logDateDir . DIRECTORY_SEPARATOR . $this->fileName;
            $this->PutContents();
            if ($this->output) {
                echo($this->log);
            }
            $this->log = '';
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws Exception
     */
    public function success($log): void
    {
        $this->writeLine($log, self::SUCCESS);
    }

    /**
     * @throws Exception
     */
    public function warning($log): void
    {
        $this->writeLine($log, self::WARNING);
    }

    /**
     * @throws Exception
     */
    public function error($log): void
    {
        $this->writeLine($log, self::ERROR);
    }

    /**
     * @throws Exception
     */
    public function critical($log): void
    {
        $this->writeLine($log, self::CRITICAL);
    }

    /**
     * @throws Exception
     */
    public function debug($log): void
    {
        $this->writeLine($log, self::DEBUG);
    }
}