<?php

declare(strict_types=1);

namespace TongkaskFrame\Component\DataBase;

class Raw
{
    public array  $map;
    public string $value;
}
