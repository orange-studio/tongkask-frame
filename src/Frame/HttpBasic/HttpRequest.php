<?php

namespace TongkaskFrame\Frame\HttpBasic;

use Exception;
use Swoole\Http\Request as SwooleHttpRequest;
use Swoole\Http\Response as SwooleHttpResponse;
use Throwable;
use TongkaskFrame\TongkaskException;
use TongkaskFrame\Tool\Instance\SnowflakeInstance;
use TongkaskFrame\Tool\UtilTool;

class HttpRequest
{
    private SwooleHttpRequest $request;
    private ?HttpResponse     $response = null;
    private string            $TraceID  = '';
    private array             $Context  = [];
    private bool              $HttpEnd  = false;

    /**
     * @throws TongkaskException
     */
    public function __construct(SwooleHttpRequest $request, ?SwooleHttpResponse $response)
    {
        $this->request = $request;
        if ($response) {
            $this->response = new HttpResponse($response);
        }
        $this->SetTraceID();
    }

    public function GetFd(): int
    {
        return $this->request->fd;
    }

    /**
     * @throws TongkaskException
     */
    private function SetTraceID(): void
    {
        try {
            $TraceID = $this->GetHeader('Trace-ID');
            if (empty($TraceID)) {
                $this->TraceID = md5(SnowflakeInstance::getInstance()->id());
            } else {
                $this->TraceID = $TraceID;
            }
            if ($this->response) {
                $this->response->SetHeader('Trace-ID', $this->TraceID);
            }
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::Logger_ERROR_CODE);
        }
    }

    /**
     * @throws Exception
     */
    public function GetHeader(string $HeaderKey)
    {
        return $this->GetHeaders()[$HeaderKey] ?? '';
    }

    /**
     * @throws Exception
     */
    public function GetHeaders()
    {
        $Headers = $this->request->header;
        $IsXXS   = UtilTool::checkXss(json_encode($Headers));
        if ($IsXXS) {
            throw new Exception('illegal request', TongkaskException::Request_Xxs_ERROR_CODE);
        }
        return $Headers;
    }

    public function IsEnd(): bool
    {
        return $this->HttpEnd;
    }

    public function End(): void
    {
        $this->HttpEnd = true;
    }

    public function GetContext(string $key = '')
    {
        if (empty($key)) {
            return $this->Context;
        }
        $res = $this->Context[$key] ?? '';
        if (is_string($res) && UtilTool::isJson($res)) {
            return json_decode($res, true);
        }
        return $res;
    }

    public function SetContext(string $key, $value): void
    {
        $this->Context[$key] = $value;
    }

    public function GetTraceID(): string
    {
        return $this->TraceID;
    }

    public function GetServer()
    {
        return $this->request->server;
    }

    public function GetResponse(): HttpResponse
    {
        return $this->response;
    }

    /**
     * @throws TongkaskException
     */
    public function GetParam(string $key)
    {
        $params = $this->GetParams();
        return $params[$key] ?? '';
    }

    /**
     * @throws TongkaskException
     * @throws Exception
     */
    public function GetParams(): array
    {
        $params = $this->request->get ?? [];
        if (count($params) > 128) {
            throw new TongkaskException('Indicates the maximum number of GET request parameters', TongkaskException::Pdo_Secure_ERROR_CODE);
        }
        $IsXXS = UtilTool::checkXss(json_encode($params));
        if ($IsXXS) {
            throw new Exception('illegal request', TongkaskException::Request_Xxs_ERROR_CODE);
        }
        return json_decode($params, true);
    }

    /**
     * @throws TongkaskException
     */
    public function PostParam(string $key)
    {
        $params = $this->PostParams();
        return $params[$key] ?? '';
    }

    /**
     * @throws TongkaskException
     * @throws Exception
     */
    public function PostParams(): array
    {
        $params = $this->request->post ?? [];
        if (count($params) > 128) {
            throw new TongkaskException('Indicates the maximum number of POST request parameters', TongkaskException::Pdo_Secure_ERROR_CODE);
        }
        $IsXXS = UtilTool::checkXss(json_encode($params));
        if ($IsXXS) {
            throw new Exception('illegal request', TongkaskException::Request_Xxs_ERROR_CODE);
        }
        return json_decode($params, true);
    }

    /**
     * @throws Exception
     */
    public function GetCookies(): array
    {
        $Cookies = $this->request->cookie ?? [];
        $IsXXS   = UtilTool::checkXss(json_encode($Cookies));
        if ($IsXXS) {
            throw new Exception('illegal request', TongkaskException::Request_Xxs_ERROR_CODE);
        }
        return json_decode($Cookies, true);
    }

    public function GetCookie(string $key)
    {
        return $this->request->cookie[$key];
    }

    /*
     * 类型为以 form 名称为 key 的二维数组。与 PHP 的 $_FILES 相同。
     * 最大文件尺寸不得超过 package_max_length 设置的值。
     * 因为 Swoole 在解析报文的时候是会占用内存的，报文越大，内存占用越大，
     * 因此请勿使用 Swoole\Http\Server 处理大文件上传或者由用户自行设计断点续传的功能。
     */
    public function GetFiles()
    {
        return $this->request->files;
    }

    /*
     * 用于非 application/x-www-form-urlencoded 格式的 HTTP POST 请求。
     * 返回原始 POST 数据，此函数等同于 PHP 的 fopen('php://input')
     */
    /**
     * @throws Exception
     */
    public function GetContent(): false|string
    {
        $Content = $this->request->rawContent();
        $IsXXS   = UtilTool::checkXss($Content);
        if ($IsXXS) {
            throw new Exception('illegal request', TongkaskException::Request_Xxs_ERROR_CODE);
        }
        return $Content;
    }

    public function GetData(): false|string
    {
        return $this->request->getData();
    }

    public function Create(array $options): SwooleHttpRequest
    {
        return $this->request->create($options);
    }

    public function Parse(string $data): false|int
    {
        return $this->request->parse($data);
    }

    public function IsCompleted(): bool
    {
        return $this->request->isCompleted();
    }

    public function GetMethod()
    {
        return $this->request->server['request_method'];
    }

    /**
     * @throws Exception
     */
    public function GetAllUri(): string
    {
        $QueryString = $this->request->server['query_string'] ?? '';
        $QueryString = !empty($QueryString) ? '?' . $QueryString : $QueryString;
        $AllUri      = $this->GetUri() . $QueryString;
        $IsXXS       = UtilTool::checkXss($AllUri);
        if ($IsXXS) {
            throw new Exception('illegal request', TongkaskException::Request_Xxs_ERROR_CODE);
        }
        return $AllUri;
    }

    public function GetUri()
    {
        return $this->request->server['request_uri'];
    }
}