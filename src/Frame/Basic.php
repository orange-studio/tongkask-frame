<?php

namespace TongkaskFrame\Frame;

use Exception;
use Inhere\Validate\Validation;
use JsonException;
use Throwable;
use TongkaskFrame\Frame\HttpBasic\HttpRequest;
use TongkaskFrame\Frame\HttpBasic\HttpResponse;
use TongkaskFrame\Struct\HttpCode;
use TongkaskFrame\Struct\HttpStatus;
use TongkaskFrame\TongkaskException;

class Basic
{
    public HttpRequest  $request;
    public HttpResponse $response;

    public function __construct(HttpRequest $request)
    {
        $this->request  = $request;
        $this->response = $request->GetResponse();
    }

    /**
     * @throws Exception
     */
    public function GetPostJsonParams(): array
    {
        $contentType = $this->request->getHeader('content-type');
        if ($this->request->getMethod() !== 'POST') {
            throw new TongkaskException('illegal request', TongkaskException::Request_Confine_ERROR_CODE);
        }
        if (!str_contains($contentType, 'application/json')) {
            throw new TongkaskException('illegal request', TongkaskException::Request_Confine_ERROR_CODE);
        }
        $paramsJson = $this->request->GetContent();
        if (empty($paramsJson)) {
            return [];
        }

        $rule    = [
            ['params', 'required', 'msg' => '{attr}必填'],
            ['params', 'json', 'msg' => '{attr}格式错误'],
        ];
        $message = [
            'params' => '参数',
        ];
        $check   = Validation::check(['params' => $paramsJson], $rule, $message);
        if ($check->isFail()) {
            throw new Exception($check->firstError(), TongkaskException::Request_Confine_ERROR_CODE);
        }
        $Params = json_decode($paramsJson, true);
        // 过滤参数中的空格
        return $this->ParamsFilter($Params);
    }

    protected function ParamsFilter($params): array
    {
        foreach ($params as $key => &$val) {
            if (is_array($val)) {
                $params[$key] = $this->ParamsFilter($val);
            }
            $val = trim($val);
        }
        return $params;
    }

    /**
     * @throws JsonException
     */
    public function ReturnExceptionJson(Throwable $th, int $code = 1): void
    {
        $HttpCode          = new HttpCode();
        $HttpCode->Code    = max($code, $th->getCode());
        $HttpCode->Message = $th->getMessage();
        $this->response->SetHeader('Content-Type', 'application/json;charset=utf-8');
        $this->response->SetHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
        $this->response->SetStatus(HttpStatus::CODE_OK);
        $this->response->WriteJson($HttpCode);
    }

    /**
     * @throws Exception
     */
    public function ReturnDownload(string $path, string $name): void
    {
        if (empty($path) || !file_exists($name)) {
            throw new Exception('文件不存在');
        }
        $this->response->SetHeader('Access-Control-Allow-Origin', '*');
        $this->response->SetHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $this->response->SetHeader('Access-Control-Allow-Credentials', 'true');
        $this->response->SetHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');

        $fileName       = $name ?: pathinfo($path, PATHINFO_FILENAME);
        $fileExtNameSub = pathinfo($path, PATHINFO_EXTENSION);
        $fileName       .= '.' . $fileExtNameSub;
        $this->response->SetHeader('Content-type', 'application/octet-stream');
        $this->response->SetHeader('Last-Modified', gmdate('D, d M Y H:i:s') . ' GMT');
        $this->response->SetHeader('Content-Transfer-Encoding', 'binary');
        $this->response->SetHeader('Content-length', filesize($path));
        $this->response->SetHeader('Content-Disposition', "filename= {$fileName}");
        $this->response->SetStatus(HttpStatus::CODE_OK);
        $this->response->write(file_get_contents($path));
    }

    /**
     * @throws JsonException
     */
    protected function ReturnJson(int $Code, string $Message, ?array $Data = null): void
    {
        $HttpCode          = new HttpCode();
        $HttpCode->Code    = $Code;
        $HttpCode->Message = $Message;
        $HttpCode->Data    = $Data;
        $this->response->SetHeader('Content-Type', 'application/json;charset=utf-8');
        $this->response->SetHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
        $this->response->SetStatus(HttpStatus::CODE_OK);
        $this->response->WriteJson($HttpCode);
    }
}