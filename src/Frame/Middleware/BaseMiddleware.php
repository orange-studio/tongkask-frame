<?php

namespace TongkaskFrame\Frame\Middleware;

use TongkaskFrame\Frame\Basic;
use TongkaskFrame\Frame\HttpBasic\HttpRequest;
use TongkaskFrame\Struct\RouteMiddle;
use TongkaskFrame\Struct\RouteStruct;
use TongkaskFrame\Tool\UtilTool;

class BaseMiddleware extends Basic
{
    public bool        $Next       = false;
    public array       $Middleware = [];
    public RouteMiddle $RouteMiddle;
    public RouteStruct $Route;
    public string      $class;
    public string      $action;

    public function __construct(HttpRequest $request, RouteStruct $Route, RouteMiddle $RouteMiddle)
    {
        $this->Route       = $Route;
        $this->Middleware  = $Route->Middleware;
        $this->RouteMiddle = $RouteMiddle;
        $this->class       = $Route->class;
        $this->action      = $Route->action;
        parent::__construct($request);
    }

    public function Next(): void
    {
        $NextMiddleware = UtilTool::findNextElement($this->Middleware, $this->RouteMiddle);
        if (!empty($NextMiddleware)) {
            $class  = $NextMiddleware->class;
            $action = $NextMiddleware->action;
            $Class  = new $class($this->request, $this->Route, $NextMiddleware);
        } else {
            $class  = $this->class;
            $action = $this->action;
            $Class  = new $class($this->request);
        }
        $Class->$action();
    }

}