<?php

namespace TongkaskFrame\Frame\Extend;

use Throwable;
use TongkaskFrame\Component\DataBase\BasePdo;
use TongkaskFrame\TongkaskException;
use TongkaskFrame\Tool\ConfTool;

class DB extends BasePdo
{
    /**
     * @throws TongkaskException
     */
    public function __construct(string $configName = 'default')
    {
        try {
            if (empty($config)) {
                if ($_ENV['ENV'] == 'PRODUCE') {
                    $config = ConfTool::GetConfig('DataBase.produce.' . $configName);
                } else {
                    $config = ConfTool::GetConfig('DataBase.develop.' . $configName);
                }
            }
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::EasyPdo_ERROR_CODE);
        }
        parent::__construct($config);
    }
}