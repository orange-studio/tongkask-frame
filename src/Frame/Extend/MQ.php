<?php

namespace TongkaskFrame\Frame\Extend;

use TongkaskFrame\TongkaskException;
use TongkaskFrame\Component\RabbitMQ;
use TongkaskFrame\Tool\UtilTool;
use TongkaskFrame\Tool\ConfTool;
use Exception;
use Throwable;

class MQ extends RabbitMQ
{
    public int    $MESSAGE_TTL;
    public int    $MESSAGE_TIMEOUT;
    public string $MESSAGE_TAG;

    public function __construct(string $configName = 'default')
    {

        try {
            $this->MESSAGE_TTL     = ConfTool::GetConfig('RabbitMQ.MESSAGE_TTL');
            $this->MESSAGE_TIMEOUT = ConfTool::GetConfig('RabbitMQ.MESSAGE_TIMEOUT');
            $this->MESSAGE_TAG     = ConfTool::GetConfig('RabbitMQ.MESSAGE_TAG');
            if ($_ENV['ENV'] == 'PRODUCE') {
                $config = ConfTool::GetConfig('RabbitMQ.produce.' . $configName);
            } else {
                $config = ConfTool::GetConfig('RabbitMQ.develop.' . $configName);
            }
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::EasyMQ_ERROR_CODE);
        }
        parent::__construct($config);
    }

    /**
     * @throws TongkaskException
     */
    public function ListenDlx(string $chanName, string $chanNameDlx, $callback, array $args = []): void
    {
        try {
            $tag           = $chanName;
            $queueName     = "{$this->queuePrefix}{$chanName}";
            $queueNameDlx  = "{$this->queuePrefix}{$chanNameDlx}";
            $routingKey    = "{$this->exchangeName}.{$queueName}";
            $routingKeyDlx = "{$this->exchangeName}.{$queueNameDlx}";
            $arg           = [
                'x-dead-letter-exchange'    => $this->exchangeName,
                'x-dead-letter-routing-key' => $routingKeyDlx,
            ];
            $args          = array_merge($arg, $args);
            $argsDlx       = [
                'x-dead-letter-exchange'    => $this->exchangeName,
                'x-dead-letter-routing-key' => $routingKey,
                'x-message-ttl'             => $this->MESSAGE_TTL,
            ];
            $this->exchangeDeclare($this->exchangeName, 'topic', true, false);
            $this->queueDeclare($queueNameDlx, true, false, $argsDlx);
            $this->queueBind($queueNameDlx, $this->exchangeName, $routingKeyDlx);
            $this->queueDeclare($queueName, true, false, $args);
            $this->queueBind($queueName, $this->exchangeName, $routingKey);
            $this->consumer($queueName, $tag, false, $callback);
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::EasyMQ_ERROR_CODE);
        }
    }

    /**
     * @throws TongkaskException
     */
    public function SendDlx(string $chanName, string $chanNameDlx, array $message, array $args = []): bool
    {
        try {
            $this->messageId = UtilTool::makeSnowFlake();
            if (empty($message) || empty($chanName)) {
                throw new Exception('MQ消息或通道名为空');
            }
            $isSendOk = false;
            //confirm ack callback function
            $this->MQChan->set_ack_handler(function () use (&$isSendOk) {
                $isSendOk = true;
            });
            //confirm nack callback function
            $this->MQChan->set_nack_handler(function () use (&$isSendOk) {
                $isSendOk = false;
            });
            if (empty($this->queuePrefix)) {
                throw new Exception('队列名不存在');
            }
            if (empty($this->exchangeName)) {
                throw new Exception('交换机不存在');
            }
            $tag           = $chanName;
            $queueName     = "{$this->queuePrefix}{$chanName}";
            $queueNameDlx  = "{$this->queuePrefix}{$chanNameDlx}";
            $routingKey    = "{$this->exchangeName}.{$queueName}";
            $routingKeyDlx = "{$this->exchangeName}.{$queueNameDlx}";
            $arg           = [
                'x-dead-letter-exchange'    => $this->exchangeName,
                'x-dead-letter-routing-key' => $routingKeyDlx,
            ];
            $args          = array_merge($arg, $args);
            $argsDlx       = [
                'x-dead-letter-exchange'    => $this->exchangeName,
                'x-dead-letter-routing-key' => $routingKey,
                'x-message-ttl'             => $this->MESSAGE_TTL,
            ];
            $this->exchangeDeclare($this->exchangeName, 'topic', true, false);
            $this->queueDeclare($queueNameDlx, true, false, $argsDlx);
            $this->queueBind($queueNameDlx, $this->exchangeName, $routingKeyDlx);
            $this->queueDeclare($queueName, true, false, $args);
            $this->queueBind($queueName, $this->exchangeName, $routingKey);
            $message = $this->AMQPMessage(json_encode($message), $this->messageId, $chanName);
            // 设置confirm消息确认模式并发送消息
            $this->MQChan->confirm_select();
            $this->publish($message, $this->exchangeName, $routingKey);
            // 等待mq服务给出确认收到消息的回复
            $this->MQChan->wait_for_pending_acks();

            return $isSendOk;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::EasyMQ_ERROR_CODE);
        }
    }
}