<?php

namespace TongkaskFrame\Frame;

use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;
use TongkaskFrame\Struct\WebsocketConnectionStruct;

class WebsocketConnection
{
    public static array $Conn = [];

    public function SetConn(Server $server, $fd, $Request, $TrackID): void
    {
        $WebsocketConnectionStruct = new WebsocketConnectionStruct();
        if (isset(self::$Conn[$fd])) {
            // 关闭该 websocket 链接
            self::$Conn[$fd]->close($fd, true);
        }

        $WebsocketConnectionStruct->Server  = $server;
        $WebsocketConnectionStruct->Request = $Request;
        $WebsocketConnectionStruct->TrackID = $TrackID;
        self::$Conn[$fd]                    = $WebsocketConnectionStruct;
    }

    public static function Close($fd): bool
    {
        $Conn = self::GetConn($fd);
        if ($Conn instanceof WebsocketConnectionStruct) {
            $Conn->Server->close($fd);
            return true;
        }
        return false;
    }

    public static function IsExist($fd): bool
    {
        return (bool)self::$Conn[$fd];
    }

    public static function SetClass(int $fd, string $Class = ''): bool
    {
        $Conn = self::GetConn($fd);
        if ($Conn instanceof WebsocketConnectionStruct) {
            $Conn->Class = $Class;
            return true;
        }
        return false;
    }

    public static function SetAction(int $fd, string $Action = ''): bool
    {
        $Conn = self::GetConn($fd);
        if ($Conn instanceof WebsocketConnectionStruct) {
            $Conn->Action = $Action;
            return true;
        }
        return false;
    }

    public static function SetFrame(int $fd, Frame $Frame): bool
    {
        $Conn = self::GetConn($fd);
        if ($Conn instanceof WebsocketConnectionStruct) {
            $Conn->Frame = $Frame;
            return true;
        }
        return false;
    }

    public static function GetConn(int $fd): WebsocketConnectionStruct|bool
    {
        return self::$Conn[$fd] ?? false;
    }
}