<?php

namespace TongkaskFrame\Frame;

use EasyTask\Task;
use TongkaskFrame\Struct\TaskStruct;

class TaskProcess
{

    private array $TaskList;

    public function SetTaskList(array $TaskList): void
    {
        $this->TaskList = $TaskList;
        foreach ($TaskList as $Task) {
            $TaskStruct             = new TaskStruct();
            $TaskStruct->name       = $Task[0];
            $TaskStruct->class      = $Task[1];
            $TaskStruct->action     = $Task[2];
            $TaskStruct->processNum = $Task[3] ?? 1;
            $TaskStruct->time       = $Task[4] ?? 0;
            $this->AddTask($TaskStruct);
        }
    }

    public function AddTask(TaskStruct $TaskStruct): void
    {
        $this->TaskList[] = $TaskStruct;
    }

    public function TaskRun(Task $task): void
    {
        foreach ($this->TaskList as $itemTask) {
            if ($itemTask instanceof TaskStruct) {
                $name       = $itemTask->name;
                $class      = $itemTask->class;
                $action     = $itemTask->action;
                $processNum = $itemTask->processNum;
                $time       = $itemTask->time;
                if (empty($name) || empty($class) || empty($action)) {
                    exit('任务配置错误');
                }
                if (!class_exists($class)) {
                    exit("Task {$class} is not exist");
                }
                if (!method_exists($class, $action)) {
                    exit("Task {$class}::{$action} is not exist");
                }

                $task->addClass($class, $action, $name, $time, $processNum);
            }

        }
    }
}