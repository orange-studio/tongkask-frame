<?php

namespace TongkaskFrame\Frame;

use Exception;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Websocket\Frame;
use Swoole\Websocket\Server;
use Throwable;
use TongkaskFrame\Component\Route\HttpRouteCollector;
use TongkaskFrame\Component\Route\WebsocketRouteCollector;
use TongkaskFrame\Frame\HttpBasic\HttpRequest;
use TongkaskFrame\Frame\HttpBasic\HttpWebsocket;
use TongkaskFrame\Struct\HttpCode;
use TongkaskFrame\Struct\RouteStruct;
use TongkaskFrame\TongkaskException;
use TongkaskFrame\Tool\ConfTool;
use TongkaskFrame\Tool\FileDirTool;
use TongkaskFrame\Tool\Instance\LOGInstance;
use TongkaskFrame\Tool\Instance\WebSocketConnectionInstance;
use TongkaskFrame\Tool\UtilTool;

class HttpServer
{
    private int                     $port        = 8077;
    private string                  $addr        = '0.0.0.0';
    private                         $HttpServerErrorCallback;
    private                         $WebsocketOpenErrorCallback;
    private HttpRouteCollector      $HttpRouteCollector;
    private WebsocketRouteCollector $WebsocketRouteCollector;
    private array                   $connections = [];

    public function SetHttpRouteList(HttpRouteCollector $route): void
    {
        $this->HttpRouteCollector = $route;
    }

    public function SetWebsocketRouteList(WebsocketRouteCollector $route): void
    {
        $this->WebsocketRouteCollector = $route;
    }

    public function SetHttpServerErrorCallback($HttpServerErrorCallback): void
    {
        $this->HttpServerErrorCallback = $HttpServerErrorCallback;
    }

    public function SetWebsocketOpenErrorCallback($WebsocketOpenErrorCallback): void
    {
        $this->WebsocketOpenErrorCallback = $WebsocketOpenErrorCallback;
    }

    /**
     * @throws TongkaskException
     */
    public function Start(): void
    {
        try {
            $this->port = ConfTool::GetConfig('server.port');
            $this->addr = ConfTool::GetConfig('server.addr');
            $server     = new Server($this->addr, $this->port, SWOOLE_PROCESS);
            // 加载 httpserver 配置
            $this->LoadServerConfig($server);
            $server->on('Request', function (Request $request, Response $response) {
                $HttpReqesrt = new HttpRequest($request, $response);
                $logger      = LOGInstance::getInstance()->SetTraceID($HttpReqesrt->GetTraceID())->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'));
                try {
                    ob_start();
                    $this->connections[$request->fd] = 'qs';
                    $this->LoadHttpRoute($HttpReqesrt);
                } catch (Throwable $th) {
                    if (empty($this->HttpServerErrorCallback)) {
                        call_user_func($this->HttpServerErrorCallback, $HttpReqesrt, $th);
                    } else {
                        $resArr          = new HttpCode();
                        $resArr->Code    = TongkaskException::System_ERROR_CODE;
                        $resArr->Message = $th->getMessage();
                        $resArr->Data    = null;
                        $response->write(json_encode($resArr));
                    }
                    $logger->appendLog($th->getMessage(), '', false);
                    $logger->appendLog($th->getTrace(), '', false);
                } finally {
                    $output = ob_get_clean();
                    if ($output) {
                        $logger->appendLog($output, '', false);
                    }
                    $logger->write(false);
                    unset($HttpReqesrt);
                    $response->end();
                }
            });
            $server->on('open', function (Server $server, Request $request) {
                $HttpWebsocket = new HttpWebsocket($request);
                $logger        = LOGInstance::getInstance()->SetTraceID($HttpWebsocket->GetTraceID())->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'));
                try {
                    $this->connections[$request->fd] = 'ws';
                    $HttpReqesrt                     = new HttpWebsocket($request);
                    WebSocketConnectionInstance::getInstance()->SetConn($server, $request->fd, $request, $HttpReqesrt->GetTraceID());
                    $RouteSturct = $this->LoadWebsocketRoute($HttpReqesrt);

                    if ($RouteSturct[0] === WebsocketRouteCollector::NOT_FOUND) {
                        $server->push($request->fd, 'NOT FOUND');
                        throw new TongkaskException('NOT FOUND');
                    }
                    if ($RouteSturct[0] === WebsocketRouteCollector::METHOD_NOT_ALLOWED) {
                        $server->push($request->fd, 'METHOD NOT ALLOWED');
                        throw new TongkaskException('METHOD NOT ALLOWED');
                    }
                    if ($RouteSturct[1] instanceof RouteStruct) {
                        $Class  = $RouteSturct[1]->class;
                        $Action = $RouteSturct[1]->action;
                        (new $Class($request))->{$Action}();
                    } else {
                        $server->push($request->fd, 'ROUTE ERR');
                        throw new TongkaskException('ROUTE ERR');
                    }
                } catch (Throwable $th) {
                    if (empty($this->WebsocketOpenErrorCallback)) {
                        call_user_func($this->WebsocketOpenErrorCallback, $server, $HttpWebsocket, $th);
                    } else {
                        $logger->appendLog($th->getMessage(), '', false);
                        $logger->appendLog($th->getTrace(), '', false);
                    }
                    WebSocketConnectionInstance::getInstance()::Close($request->fd);
                    $server->close($request->fd);
                } finally {
                    $logger->write(false);
                }
            });
            $server->on('message', function (Server $server, Frame $frame) {
                $WebsocketConn = WebSocketConnectionInstance::getInstance()::GetConn($frame->fd);
                $logger        = LOGInstance::getInstance()->SetTraceID($WebsocketConn->TrackID)->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'));
                try {
                    ob_start();
                    WebSocketConnectionInstance::getInstance()->SetFrame($frame->fd, $frame);
                    $WebsocketConn = WebSocketConnectionInstance::getInstance()::GetConn($frame->fd);
                    if (empty($WebsocketConn->Class) || empty($WebsocketConn->Action)) {
                        throw new TongkaskException('未设置消息处理方法');
                    }
                    $Class  = new $WebsocketConn->Class($WebsocketConn->Request);
                    $Action = $WebsocketConn->Action;
                    $Class->$Action();
                } catch (Throwable $th) {
                    $logger->appendLog($th->getMessage(), '', false);
                    $logger->appendLog($th->getTrace(), '', false);
                    $server->push($frame->fd, $th->getMessage());
                } finally {
                    $output = ob_get_clean();
                    if ($output) {
                        $logger->appendLog($output, '', false);
                    }
                    $logger->write(false);
                }
            });
            $server->on('close', function (Server $server, int $fd) {
                $logger = LOGInstance::getInstance()->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'));
                try {
                    ob_start();
                    if ($this->connections[$fd] == 'ws') {
                        WebSocketConnectionInstance::getInstance()->Close($fd);
                        $logger->appendLog('WebsocketClose:' . "[$fd] is closed", '');
                    }
                    unset($this->connections[$fd]);
                } catch (Throwable $th) {
                    $logger->appendLog($th->getMessage(), '', false);
                    $logger->appendLog($th->getTrace(), '', false);
                } finally {
                    $output = ob_get_clean();
                    if ($output) {
                        $logger->appendLog($output, '', false);
                    }
                    $logger->write(false);
                }

            });
            $server->on('Shutdown', function ($server) {
                var_dump('Shutdown');
            });
            $server->start();
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::System_ERROR_CODE);
        }
    }

    /**
     * @throws Exception
     */
    private function LoadServerConfig(Server $http): void
    {
        try {
            $httpConfig = ConfTool::GetConfig('swoole');
            foreach ($httpConfig as $key => &$value) {
                if ($key == 'log_file') {
                    $value = UtilTool::NormalizedPath(TONGKASK_ROOT . DIRECTORY_SEPARATOR . $value);
                    FileDirTool::touchFile($value);
                }
                if ($key == 'pid_file') {
                    $value = UtilTool::NormalizedPath(TONGKASK_ROOT . DIRECTORY_SEPARATOR . $value);
                    FileDirTool::touchFile($value);
                }
            }
            unset($value);
            $http->set($httpConfig);
        } catch (Throwable $th) {
            throw new Exception($th->getMessage(), TongkaskException::System_ERROR_CODE);
        }
    }

    /**
     * @throws TongkaskException
     */
    private function LoadHttpRoute(HttpRequest $request): void
    {
        try {
            // 获取请求方法和URI
            $method    = $request->GetMethod();
            $uri       = $request->GetUri();
            $routeInfo = $this->HttpRouteCollector->Dispatch($request, $uri, $method);
            switch ($routeInfo[0]) {
                case HttpRouteCollector::NOT_FOUND:
                    $request->GetResponse()->SetStatus(404);
                    $request->GetResponse()->Write('Not found');
                    break;
                case HttpRouteCollector::METHOD_NOT_ALLOWED:
                    $request->GetResponse()->SetStatus(405);
                    $request->GetResponse()->Write('Method not allowed');
                    break;
                case HttpRouteCollector::FOUND:
                    // 执行路由处理程序
                    if ($routeInfo[1] instanceof RouteStruct) {
                        $this->RouteHandle($request, $routeInfo[1]);
                    }
                    break;
            }
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::Router_ERROR_CODE);
        }
    }

    /**
     * @throws TongkaskException
     */
    private function RouteHandle(HttpRequest $request, RouteStruct $route): void
    {
        try {
            if (!empty($route->Middleware) && count($route->Middleware) > 0) {
                $FirstMiddleware = UtilTool::findNextElement($route->Middleware);
                $class           = $FirstMiddleware->class;
                $action          = $FirstMiddleware->action;
                if (!empty($class) && !empty($action)) {
                    $Class = new $class($request, $route, $FirstMiddleware);
                    $Class->$action(function () use ($request, $route) {
                        $Class  = $route->class;
                        $action = $route->action;
                        $Class  = new $Class($request);
                        $Class->$action();
                    });
                }
            } else {
                $Class  = $route->class;
                $action = $route->action;
                $Class  = new $Class($request);
                $Class->$action();
            }
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::System_ERROR_CODE);
        }
    }

    private function LoadWebsocketRoute(HttpWebsocket $httpWebsocket): array
    {
        // 获取请求方法和URI
        $method = $httpWebsocket->GetMethod();
        $uri    = $httpWebsocket->GetUri();
        return $this->WebsocketRouteCollector->Dispatch($httpWebsocket, $uri, $method);
    }

    /**
     * @Description 请求记录输出控制台
     * @throws TongkaskException
     */
    public function OutConsoleRequestLog(HttpRequest $request): void
    {
        try {
            $method      = $request->getMethod();
            $statusCode  = $request->GetResponse()->GetStatus();
            $url         = $request->GetAllUri();
            $TraceID     = $request->GetTraceID();
            $RequestInfo = "{$statusCode} [{$method}] {$url}\n";
            $logger      = LOGInstance::getInstance()->SetInitFileConfig(ConfTool::GetConfig('Log.system_log'))->SetTraceID($TraceID);
            $logger->appendLog($RequestInfo);
            $logger->write(false);
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), TongkaskException::System_ERROR_CODE);
        }
    }
}