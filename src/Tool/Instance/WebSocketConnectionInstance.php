<?php

namespace TongkaskFrame\Tool\Instance;

use Throwable;
use TongkaskFrame\Frame\WebsocketConnection;
use TongkaskFrame\TongkaskException;

class WebSocketConnectionInstance
{
    protected static ?WebsocketConnection $_instance = null;

    /**
     * @throws TongkaskException
     */
    public static function getInstance(): WebsocketConnection
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new WebsocketConnection();
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), $th->getCode());
        }
    }
}