<?php

namespace TongkaskFrame\Tool\Instance;

use TongkaskFrame\Component\Route\HttpRouteCollector;

class RouterHttpInstance
{
    protected static ?HttpRouteCollector $_instance = null;

    public static function getInstance(): HttpRouteCollector
    {
        if (self::$_instance === null) {
            self::$_instance = new HttpRouteCollector();
        }

        return self::$_instance;
    }
}