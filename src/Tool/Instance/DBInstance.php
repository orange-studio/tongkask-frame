<?php

namespace TongkaskFrame\Tool\Instance;

use TongkaskFrame\TongkaskException;
use TongkaskFrame\Frame\Extend\DB;
use Throwable;

class DBInstance
{
    protected static ?DB $_instance = null;

    /**
     * @throws TongkaskException
     */
    public static function getInstance(string $configName = 'default'): DB
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new DB($configName);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), $th->getCode());
        }
    }
}