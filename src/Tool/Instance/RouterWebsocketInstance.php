<?php

namespace TongkaskFrame\Tool\Instance;

use TongkaskFrame\Component\Route\WebsocketRouteCollector;

class RouterWebsocketInstance
{
    protected static ?WebsocketRouteCollector $_instance = null;

    public static function getInstance(): WebsocketRouteCollector
    {
        if (self::$_instance === null) {
            self::$_instance = new WebsocketRouteCollector();
        }

        return self::$_instance;
    }
}