<?php

namespace TongkaskFrame\Tool\Instance;

use Godruoyi\Snowflake\Snowflake;
use Throwable;
use TongkaskFrame\TongkaskException;
use TongkaskFrame\Tool\ConfTool;

class SnowflakeInstance
{
    protected static ?Snowflake $_instance = null;

    /**
     * @throws TongkaskException
     */
    public static function getInstance(): Snowflake
    {
        try {
            if (self::$_instance === null) {
                $dataCenterId    = ConfTool::GetConfig('Uuid.DataCenterId');
                $workerId        = ConfTool::GetConfig('Uuid.Workerid');
                self::$_instance = new Snowflake($dataCenterId, $workerId);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), $th->getCode());
        }
    }
}