<?php

namespace TongkaskFrame\Tool\Instance;

use TongkaskFrame\TongkaskException;
use TongkaskFrame\Frame\Extend\MQ;
use Throwable;

class MQInstance
{
    protected static ?MQ $_instance = null;

    /**
     * @throws TongkaskException
     */
    public static function getInstance(string $configName = 'default'): MQ
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new MQ($configName);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), $th->getCode());
        }
    }
}