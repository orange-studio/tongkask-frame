<?php

namespace TongkaskFrame\Tool\Instance;

use TongkaskFrame\TongkaskException;
use TongkaskFrame\Frame\Extend\LOG;
use Throwable;

class LOGInstance
{
    protected static ?LOG $_instance = null;

    /**
     * @throws TongkaskException
     */
    public static function getInstance(): LOG
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new LOG();
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), $th->getCode());
        }
    }
}