<?php

namespace TongkaskFrame\Tool\Instance;

use TongkaskFrame\TongkaskException;
use TongkaskFrame\Frame\Extend\REDIS;
use Throwable;

class REDISInstance
{
    protected static ?REDIS $_instance = null;

    /**
     * @throws TongkaskException
     */
    public static function getInstance(string $configName = 'default'): REDIS
    {
        try {
            if (self::$_instance === null) {
                self::$_instance = new REDIS($configName);
            }
            return self::$_instance;
        } catch (Throwable $th) {
            throw new TongkaskException($th->getMessage(), $th->getCode());
        }
    }
}