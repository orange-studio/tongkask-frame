<?php

namespace TongkaskFrame\Tool;

use Symfony\Component\Yaml\Yaml;
use TongkaskFrame\TongkaskException;

class ConfTool
{

    /**
     * @throws TongkaskException
     */
    public static function GetConfig(string $key)
    {
        $KeyArr = explode('.', $key);
        $Config = self::GetConfigs();
        foreach ($KeyArr as $key) {
            if (empty($key)) {
                continue;
            }
            if (empty($Config[$key]) && $Config[$key] !== 0 && $Config[$key] !== false) {
                throw new TongkaskException("The configuration {$key} does not exist");
            }
            $Config = $Config[$key];
        }
        return $Config;
    }

    /**
     * 获取 YAML 配置文件内容
     *
     * @return mixed
     */
    public static function GetConfigs(): mixed
    {
        return Yaml::parseFile(TONGKASK_CONFIG_ROOT);
    }
}