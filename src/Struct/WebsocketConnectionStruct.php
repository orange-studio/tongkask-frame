<?php

namespace TongkaskFrame\Struct;

use Swoole\Http\Request;
use Swoole\WebSocket\Frame;
use Swoole\Websocket\Server;

class WebsocketConnectionStruct
{
    public string  $TrackID;
    public Frame   $Frame;
    public int     $Fd;
    public Server  $Server;
    public string  $Class;
    public string  $Action;
    public Request $Request;
}