<?php

namespace TongkaskFrame\Struct;

class RouteStruct
{
    public array  $Middleware = [];
    public string $method     = '';
    public string $path       = '';
    public string $class      = '';
    public string $action     = '';
}