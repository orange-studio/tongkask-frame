<?php

namespace TongkaskFrame\Struct;

class TaskStruct
{
    public string $name;
    public        $class;
    public string $action;
    public int    $processNum = 1;
    public int    $time       = 0;
}